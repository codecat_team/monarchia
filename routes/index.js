function renderView(view, body){
    return function(req,res,next){
        res.render(view, body);
    }
}

function e408(req,res,next){
    res.status(408);
    res.render('errors/408');
}

module.exports = function(app){
    app.get('/', renderView('index'));
    app.get('/mpt', renderView('mpt'))
    app.get('/inne', e408);
};
