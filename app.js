var express = require('express');
var compress = require('compression');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var http = require('http');
var fs = require('fs');
var flash = require('express-flash');
var sassMiddleware = require('node-sass-middleware');
var session = require('express-session')

var PORT = process.env.PORT || 8000;

var app = express();

var Server = http.createServer(app);

app.set('port', PORT);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.locals.pretty = true;

app.use(session({
    secret: 'keyboard cat',
    //resave: false,
    //saveUninitialized: true,
    cookie: { maxAge: 60000 }
}));

app.use(logger(':method :date[iso] :url :status :response-time ms - :res[content-length]'));
app.use(compress());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(sassMiddleware({
    src: path.join(__dirname,'scss'),
    dest: path.join(__dirname, 'public/css'),
    debug: true,
    prefix: '/css'
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(flash());

require('./routes/index')(app);

var server = Server.listen(app.get('port'), function() {
    console.log('Express server listening on port ' + server.address().port);
});
